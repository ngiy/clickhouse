FROM yandex/clickhouse-server:20.12.3.3

COPY config.xml /etc/clickhouse-server/config.xml
COPY metrika.xml /etc/clickhouse-server/metrika.xml
COPY macro01.xml /etc/clickhouse-server/config.d/macros.xml
